//
//  IncomeTaxCalculatorTests.m
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "IncomeTaxCalculator.h"

@interface IncomeTaxCalculatorTests : XCTestCase

@end

@implementation IncomeTaxCalculatorTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPayableTax {
    IncomeTaxCalculator *calculator = [[IncomeTaxCalculator alloc] init];
    
    [calculator calculatePayableTaxBasedOnGrossIncome:60050 completion:^(CGFloat taxPayable, CGFloat netIncome) {
        NSInteger taxRounded = round(taxPayable);
        XCTAssert(11063 == taxRounded, @"Calculated value is: %ld", taxRounded);
    }];
}

- (void)testNetIncome {
    IncomeTaxCalculator *calculator = [[IncomeTaxCalculator alloc] init];
    
    [calculator calculatePayableTaxBasedOnGrossIncome:60050 completion:^(CGFloat taxPayable, CGFloat netIncome) {
        NSInteger incomeRounded = round(netIncome);
        XCTAssert(48987 == incomeRounded, @"Calculated value is: %ld", incomeRounded);
    }];
}


@end
