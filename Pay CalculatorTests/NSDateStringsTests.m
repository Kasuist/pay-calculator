//
//  NSDateStringsTests.m
//  Pay Calculator
//
//  Created by Beau on 31/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "NSDate+Strings.h"

@interface NSDateStringsTests : XCTestCase

@end

@implementation NSDateStringsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testMonthString {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:1422687090.212581];
    
    NSString *month = [date monthString];
    XCTAssert([month isEqualToString:@"January"]);
}

- (void)testFirstAndLastDayOfMonthString {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:1422687090.212581];
    
    NSString *firstAndLastDays = [date firstAndLastDayOfMonthString];
    
    XCTAssert([firstAndLastDays isEqualToString:@"01 January - 31 January"]);
}


@end
