##Payslip Creator

This app will generate a payslip based on a users gross annual income and super that can then be saved as a pdf and shared via email.

![IMG_1165.PNG](https://bitbucket.org/repo/nAEon7/images/575574278-IMG_1165.PNG)

Tax rates are based on the following table, but the app has been designed as such for it to be easy to swap in a new one. Perhaps even fetching one from an online source.


| Taxable income     | Tax on this income                         |
|--------------------|--------------------------------------------|
| 0 - $18,200        | Nil                                        |
| $18,201 - $37,000  | 19c for each $1 over $18,200               |
| $37,001 - $80,000  | $3,572 plus 32.5c for each $1 over $37,000 |
| $80,001 - $180,000 | $17,547 plus 37c for each $1 over $80,000  |
| $180,001 and over  | $54,547 plus 45c for each $1 over $180,000 |


I've also included code for generating the payslip with regular draw methods. 
This allows for easily presenting the calculated results in a many number of ways, while not being restricted to one particular view.

Unit tests have also been written for the math used within the app.


##Usage
Requires Mac with Xcode installed along with iOS SDK 7.0 or newer.
The app will run in the simulator but due to simulator restrictions, sending an email will not work. If you wish to test this feature you will need to run the app on a device.

You should be able to just clone this repo, open the project file and run.

##Assumptions

* This entire app is based on the very large assumption that users are only going to want to create a payslip for any given month. What would be better would be to allow users to also choose weekly, and fortnightly. Perhaps even being able to input their own range of dates entirely.

* Everything is to be rounded to the nearest dollar.

## Testing

###Input Values

| First Name | Last Name | Gross Annual Salary | Super Rate | Pay Period |
|------------|-----------|---------------------|------------|------------|
| Beau       | Young     | 80000               | 10         | January    |
| John       | Smith     | 98000               | 8          | March       |
| Tahlia     | Young    | 22000             | 5           | July     |


###Expected Output 

| Name         | Pay Period              | Gross     | Tax       | Net       | Super   |
|--------------|-------------------------|-----------|-----------|-----------|---------|
| Beau Young   | 01 January - 31 January | $6,666.00 | $1,462.00 | $5,204.00 | $666.00 |
| John Smith   | 01 March - 31 March     | $8,166.00 | $2,017.00 | $6,149.00 | $653.00 |
| Tahlia Young | 01 July - 31 July       | $1,833.00 | $60.00    | $1,773.00 | $91.00  |