//
//  CalculateButton.m
//  Pay Calculator
//
//  Created by Beau on 29/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "CalculateButton.h"

@implementation CalculateButton

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = 5.0;
    self.layer.borderColor = self.titleLabel.textColor.CGColor;
    self.layer.borderWidth = 1.0;
}

@end
