//
//  CalculateButton.h
//  Pay Calculator
//
//  Created by Beau on 29/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 Just a custom button with rounded border.
 */
@interface CalculateButton : UIButton

@end
