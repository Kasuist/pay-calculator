//
//  MonthPicker.h
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MonthPickerDelegate;

@interface MonthPicker : UIPickerView <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, weak) id <MonthPickerDelegate> _delegate;
@property (nonatomic, strong) NSDate *date;

- (void)selectToday;

@end

@protocol MonthPickerDelegate <NSObject>

@optional
- (void)pickerView:(UIPickerView *)pickerView didChangeDate:(NSDate*)newDate;
@end