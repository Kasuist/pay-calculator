//
//  PayslipView.m
//  Pay Calculator
//
//  Created by Beau on 31/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "PayslipView.h"
#import "PaySlipStyleKit.h"

@implementation PayslipView


- (void)setResult:(Result *)result {
    _result = result;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [PaySlipStyleKit drawPayslipWithName:_result.fullName
                               payPeriod:_result.payPeriod
                             grossIncome:_result.grossIncome
                                     tax:_result.taxPayable
                               netIncome:_result.netIncome
                             superIncome:_result.superannuation];
}

@end
