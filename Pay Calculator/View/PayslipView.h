//
//  PayslipView.h
//  Pay Calculator
//
//  Created by Beau on 31/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Result.h"

@interface PayslipView : UIView

@property (strong, nonatomic) Result *result;

@end
