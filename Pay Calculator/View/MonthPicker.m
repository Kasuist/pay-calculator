//
//  MonthPicker.m
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "MonthPicker.h"

// Identifiers of components
#define MONTH ( 0 )

// Identifies for component views
#define LABEL_TAG 43

@interface MonthPicker()

@property (nonatomic, strong) NSIndexPath *todayIndexPath;
@property (nonatomic, strong) NSArray *months;

@end

@implementation MonthPicker

const NSInteger bigRowCount = 1;
const CGFloat rowHeight = 44.f;
const NSInteger numberOfComponents = 1;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.months = [self nameOfMonths];
    self.todayIndexPath = [self todayPath];
    
    self.delegate = self;
    self.dataSource = self;
}

- (void)setDate:(NSDate *)aDate {
    [self setDate:aDate animated:NO];
}

- (void)setDate:(NSDate *)aDate animated:(BOOL)animated {
    
    NSInteger dateMonth;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:aDate];
    dateMonth = [components month];
    
    [self selectRow:dateMonth - 1 inComponent:MONTH animated:NO];
}

- (NSDate *)date {
    NSInteger monthCount = [self.months count];
    NSString *month = [self.months objectAtIndex:([self selectedRowInComponent:MONTH] % monthCount)];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init]; [formatter setDateFormat:@"MMMM"];
    NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@", month]];
    
    return date;
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return [self componentWidth];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    BOOL selected = NO;
    
    NSInteger monthCount = [self.months count];
    NSString *monthName = [self.months objectAtIndex:(row % monthCount)];
    NSString *currentMonthName = [self currentMonthName];
    if([monthName isEqualToString:currentMonthName] == YES) {
        selected = YES;
    }
    
    UILabel *returnView = nil;
    
    if (view.tag == LABEL_TAG) {
        returnView = (UILabel *)view;
    }
    else {
        returnView = [self labelForComponent: component selected: selected];
    }
    
    returnView.text = [self titleForRow:row forComponent:component];
    return returnView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return rowHeight;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    id <MonthPickerDelegate> strongDelegate = self._delegate;
    if ([strongDelegate respondsToSelector:@selector(pickerView:didChangeDate:)]) {
        [strongDelegate pickerView:self didChangeDate:self.date];
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return numberOfComponents;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self bigRowMonthCount];
}

#pragma mark - Util

- (NSInteger)bigRowMonthCount {
    return [self.months count]  * bigRowCount;
}

- (CGFloat)componentWidth {
    return self.bounds.size.width / numberOfComponents;
}

- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSInteger monthCount = [self.months count];
    return [self.months objectAtIndex:(row % monthCount)];
}

- (UILabel *)labelForComponent:(NSInteger)component selected:(BOOL)selected {
    CGRect frame = CGRectMake(0.f, 0.f, [self componentWidth],rowHeight);
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18.f];
    label.userInteractionEnabled = NO;
    label.tag = LABEL_TAG;
    
    return label;
}

- (NSArray *)nameOfMonths {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    return [dateFormatter standaloneMonthSymbols];
}

- (void)selectToday {
    [self selectRow:self.todayIndexPath.row inComponent:MONTH animated:NO];
}

-(NSIndexPath *)todayPath {
    CGFloat row = 0.f;
    CGFloat section = 0.f;
    
    NSString *month = [self currentMonthName];
    
    for(NSString *cellMonth in self.months) {
        if([cellMonth isEqualToString:month]) {
            row = [self.months indexOfObject:cellMonth];
            row = row + [self bigRowMonthCount] / 2;
            break;
        }
    }
    return [NSIndexPath indexPathForRow:row inSection:section];
}

- (NSString *)currentMonthName {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM"];
    return [formatter stringFromDate:[NSDate date]];
}

@end