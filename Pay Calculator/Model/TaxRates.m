//
//  TaxRates.m
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "TaxRates.h"

@implementation TaxRates

+ (instancetype)sharedTaxRates {
    static TaxRates *sharedTaxRates = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedTaxRates = [[self alloc] init];
    });
    return sharedTaxRates;
}

- (NSArray *)taxRates {
    /* We will use local store here.
       In most cases we would have an online version that can be fetched
       and used to update the local version so that the app always remains up to date.
    */
    
    NSString *taxRatesPLIST = [[NSBundle mainBundle] pathForResource:@"TaxRates" ofType:@"plist"];
    NSArray *storedTaxRates = [NSArray arrayWithContentsOfFile:taxRatesPLIST];
    
    // Generate some tax rates
    NSMutableArray *taxRateObjects = [NSMutableArray array];
    for (NSDictionary *taxRateAttributes in storedTaxRates) {
        ATaxRate *aTaxRate = [ATaxRate aTaxRateWithAttributes:taxRateAttributes];
        [taxRateObjects addObject:aTaxRate];
    }
    
    return [taxRateObjects mutableCopy];
}

@end
