//
//  IncomeTaxCalculator.h
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TaxRates.h"

@interface IncomeTaxCalculator : NSObject

/*!
 This will calculate how much tax is payable on an annual income using a tax rate table from the ATO.
 @param grossIncome This should be the annual gross income.
 @param completion  After the calculation you'll have access to how much tax is payable as well as income after tax.
 */
- (void)calculatePayableTaxBasedOnGrossIncome:(CGFloat)grossIncome completion:(void (^)(CGFloat taxPayable, CGFloat netIncome))completion;

@end
