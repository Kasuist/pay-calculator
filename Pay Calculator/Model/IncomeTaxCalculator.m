//
//  IncomeTaxCalculator.m
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "IncomeTaxCalculator.h"

@implementation IncomeTaxCalculator

- (void)calculatePayableTaxBasedOnGrossIncome:(CGFloat)grossIncome completion:(void (^)(CGFloat taxPayable, CGFloat netIncome))completion {
    
    ATaxRate *taxRate = [self appropriateTaxRateForIncome:grossIncome];
    
    // Convert the cents per dollar to something useable.
    CGFloat perDollarPercentage = taxRate.centsPerDollarOverInitialAmount * 0.01;
    
    NSInteger taxableAmount = 0;
    if (taxRate.lowerLimit != 0) {
        taxableAmount = grossIncome - (taxRate.lowerLimit - 1);
    }
    else {
        taxableAmount = grossIncome;
    }
    
    CGFloat incomeTax = taxRate.initialAmount + (taxableAmount * perDollarPercentage);
    CGFloat netIncome = grossIncome - incomeTax;
    
    completion(incomeTax, netIncome);
}

- (ATaxRate *)appropriateTaxRateForIncome:(NSInteger)income {
    NSArray *taxRates = [[TaxRates sharedTaxRates] taxRates];
    ATaxRate *taxRate;
    
    for (ATaxRate *storedTaxRate in taxRates) {

        if (income >= storedTaxRate.lowerLimit && income <= storedTaxRate.upperLimit) {
            taxRate = storedTaxRate;
            break;
        }
        
        // special if statement for the top tax bracket since it has no upperLimit.
        if (income >= storedTaxRate.lowerLimit && storedTaxRate.upperLimit == 0) {
            taxRate = storedTaxRate;
            break;
        }
    }
    NSLog(@"Between %ld AND %ld", taxRate.lowerLimit, taxRate.upperLimit);
    
    return taxRate;
}

@end
