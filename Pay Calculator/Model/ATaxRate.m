//
//  ATaxRate.m
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "ATaxRate.h"

@implementation ATaxRate

+ (instancetype)aTaxRateWithAttributes:(NSDictionary *)attributes {
    return [[self alloc] initWithAttributes:attributes];
}

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (self) {
        
        _lowerLimit = round([attributes[@"min"] integerValue]);
        _upperLimit = round([attributes[@"max"] integerValue]);
        _initialAmount = round([attributes[@"upfront"] integerValue]);
        _centsPerDollarOverInitialAmount = [attributes[@"centsForEveryDollar"] floatValue];
        
    }
    return self;
}

@end
