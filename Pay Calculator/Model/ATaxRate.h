//
//  ATaxRate.h
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ATaxRate : NSObject

@property (nonatomic, readonly) NSInteger lowerLimit;
@property (nonatomic, readonly) NSInteger upperLimit;
@property (nonatomic, readonly) NSInteger initialAmount;
@property (nonatomic, readonly) CGFloat centsPerDollarOverInitialAmount;

/*!
 This is the preferred convenience method for initializing this class.
 @param attributes A dictionary of attributes used to set the properties of this class.
 @return itself, the ATaxRate object that was just created.
 */
+ (instancetype)aTaxRateWithAttributes:(NSDictionary *)attributes;

@end
