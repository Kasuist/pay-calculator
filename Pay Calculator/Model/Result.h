//
//  Result.h
//  Pay Calculator
//
//  Created by Beau on 31/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Result : NSObject

@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *payPeriod;
@property (strong, nonatomic) NSString *grossIncome;
@property (strong, nonatomic) NSString *netIncome;
@property (strong, nonatomic) NSString *taxPayable;
@property (strong, nonatomic) NSString *superannuation;

@end
