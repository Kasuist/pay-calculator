//
//  TaxRates.h
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATaxRate.h"

@interface TaxRates : NSObject

// We'll create a singleton for this class so that the rates will always be accessible throughout the lifespan of the app. Saves cycles by not having to parse the list for every calculation made. They can either be local, or an updated version fetched from a remote server.
+ (instancetype)sharedTaxRates;

/*!
 This will get the tax rates (currently from the app bundle) and return them via a neat array.
 @return An array of tax rate objects.
 */
- (NSArray *)taxRates;

@end
