//
//  NSDate+Strings.m
//  Pay Calculator
//
//  Created by Beau on 29/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "NSDate+Strings.h"

@implementation NSDate (Strings)

- (NSString *)monthString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MMMM";
    
    return [formatter stringFromDate:self];
}

- (NSString *)firstAndLastDayOfMonthString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"MMMM";
    
    NSString *monthName = [formatter stringFromDate:self];
    NSInteger lastDayOfMonth = [self numberOfDaysInMonth];
    
    return [NSString stringWithFormat:@"01 %@ - %@ %@", monthName, @(lastDayOfMonth), monthName];
}

- (NSInteger)numberOfDaysInMonth {
    
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSRange daysRange = [currentCalendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:self];
    
    return daysRange.length;
}

@end
