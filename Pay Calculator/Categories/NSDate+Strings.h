//
//  NSDate+Strings.h
//  Pay Calculator
//
//  Created by Beau on 29/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Strings)

- (NSString *)monthString;
- (NSString *)firstAndLastDayOfMonthString;

@end