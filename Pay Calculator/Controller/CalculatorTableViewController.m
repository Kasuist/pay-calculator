//
//  CalculatorTableViewController.m
//  Pay Calculator
//
//  Created by Beau on 28/01/2015.
//  Copyright (c) 2015 Beau Young. All rights reserved.
//

#import "CalculatorTableViewController.h"
#import "CalculateButton.h"
#import "NSDate+Strings.h"
#import "IncomeTaxCalculator.h"
#import "MonthPicker.h"
#import "PayslipView.h"
#import <MessageUI/MessageUI.h>

@interface CalculatorTableViewController ()<UITextFieldDelegate, MonthPickerDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *shareButton;

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *annualSalaryTextField;
@property (weak, nonatomic) IBOutlet UITextField *superRateTextField;

@property (weak, nonatomic) IBOutlet UITextField *payPeriodTextField;
@property (strong, nonatomic) IBOutlet MonthPicker *monthPicker;

@property (weak, nonatomic) IBOutlet PayslipView *payslipView;

@property (strong, nonatomic) Result *result;

@end

@implementation CalculatorTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Setup date picker and textField.
    self.monthPicker._delegate = self;
    self.payPeriodTextField.inputView = self.monthPicker;
    NSDate *date = [NSDate date];
    self.payPeriodTextField.text = [date firstAndLastDayOfMonthString];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 5;
    }
    if (section == 1) {
        return 2;
    }
    return 0;
}

#pragma mark - Actions
- (IBAction)tappedAnywhere:(UITapGestureRecognizer *)sender {
    // dismiss keyboard
    [self.view endEditing:YES];
}

- (IBAction)calculateTapped:(CalculateButton *)sender {
    // check if all fields are filled out.
    if ([self allParametersFilled]) {
        [self.view endEditing:YES];
        [self performCalculations];
    }
    else {
        self.shareButton.enabled = NO;

        // Inform user when fields are empty
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"You must fill out all fields before we can generate your payslip" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

- (IBAction)emailTapped:(UIBarButtonItem *)sender {
    // Generate PDF from payslipView
    NSMutableData* pdfData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0.0f, 0.0f, 792.0f, 612.0f), nil);
    
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    CGContextScaleCTM(pdfContext, 0.773f, 0.773f);
    
    [self.payslipView.layer renderInContext:pdfContext];
    
    UIGraphicsEndPDFContext();
    
    // Prepare for email
    NSString *subject = [NSString stringWithFormat:@"Payslip for %@ %@", _firstNameTextField.text, _lastNameTextField.text];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:subject];
    [mc addAttachmentData:pdfData mimeType:@"application/pdf" fileName:@"Payslip.pdf"];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

#pragma mark - Calculation

- (void)performCalculations {
    
    NSInteger grossIncome = [_annualSalaryTextField.text integerValue];
    
    IncomeTaxCalculator *incomeTaxCalc = [[IncomeTaxCalculator alloc] init];
    [incomeTaxCalc calculatePayableTaxBasedOnGrossIncome:grossIncome completion:^(CGFloat taxPayable, CGFloat netIncome) {
        
        // setup a number formatter for currency formatting.
        NSNumberFormatter *currencyFormat = [[NSNumberFormatter alloc] init];
        [currencyFormat setNumberStyle: NSNumberFormatterCurrencyStyle];
        
        // work out super
        CGFloat superPercentage = [_superRateTextField.text floatValue] * 0.01;
        NSInteger superPay = round(grossIncome * superPercentage);
        
        // round off numbers and divide by number of months
        NSInteger grossIncomeForMonth = round(grossIncome/12);
        NSInteger netIncomeForMonth = round(netIncome/12);
        NSInteger taxForMonth = round(taxPayable/12);
        NSInteger superPayForMonth = round(superPay/12);
        
        _result = [[Result alloc] init];
        _result.fullName = [NSString stringWithFormat:@"%@ %@", _firstNameTextField.text, _lastNameTextField.text];
        _result.payPeriod = _payPeriodTextField.text;
        _result.grossIncome = [currencyFormat stringFromNumber:@(grossIncomeForMonth)];
        _result.netIncome = [currencyFormat stringFromNumber:@(netIncomeForMonth)];
        _result.taxPayable = [currencyFormat stringFromNumber:@(taxForMonth)];
        _result.superannuation = [currencyFormat stringFromNumber:@(superPayForMonth)];
        
        [self renderPayslip];
        
        self.shareButton.enabled = YES;
    }];
}

#pragma mark - Payslip Render

- (void)renderPayslip {
    [self.payslipView setResult:_result];
}

#pragma mark - Month Picker Delegate
- (void)pickerView:(UIPickerView *)pickerView didChangeDate:(NSDate *)newDate {
    NSLog(@"DATE CHANGED");
    self.payPeriodTextField.text = [newDate firstAndLastDayOfMonthString];
}

#pragma mark - Helpers

- (BOOL)allParametersFilled {
    if (_firstNameTextField.text.length > 0 && _lastNameTextField.text.length > 0 &&
        _annualSalaryTextField.text.length > 0 && _superRateTextField.text.length > 0 && _payPeriodTextField.text > 0) {
        
        return YES;
    }
    return NO;
}

#pragma mark - Mail Composer Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
